<?php

namespace Drupal\pfr;

use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;

/**
 * Provides a listing of Permission filter by role entities.
 */
class PermissionFilterByRoleListBuilder extends ConfigEntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['label'] = $this->t('Permission filter by role');
    $header['id'] = $this->t('Machine name');
    $header['module_permissions'] = $this->t('Permissions');
    $header['roles_permissions'] = $this->t('Roles Access');

    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    $row['label'] = $entity->label();
    $row['id'] = $entity->id();
    $row['module_permissions'] = $this->replace_string($entity->getModulePermissions());
    $row['roles_permissions'] = $this->replace_string($entity->getRolesPermissions());
    return $row + parent::buildRow($entity);
  }

  public function replace_string($str) {
    if ($str == '-1') {
      $str = $this->t("All Access");
    }else{
      if($str == '-2'){
        $str = $this->t("None Access");
      }else{
        $str = str_replace("," , ", " , $str);
      }
    }
    return $str;
  }

}
