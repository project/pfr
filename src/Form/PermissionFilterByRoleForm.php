<?php

namespace Drupal\pfr\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\user\Entity\Role;
use Drupal\user\PermissionHandlerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class PermissionFilterByRoleForm.
 */
class PermissionFilterByRoleForm extends EntityForm {
  /**
   * Indicates that all options should be user for filter.
   */
  const ALL_OPTIONS = '-1';
  const NONE_OPTIONS = '-2';


  protected $permission_handler;

  public function __construct(PermissionHandlerInterface $permission_handler) {
    $this->permission_handler =$permission_handler;
  }

  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('user.permissions')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $permission_filter_by_role = $this->entity;

    $roles = Role::loadMultiple();
    $permissions = $this->permission_handler->getPermissions();
    $defined_roles = [];
    foreach ($roles as $role_name => $role) {
      $defined_roles[$role_name] = $role->label();
    }

    $providers = [];
    foreach ($permissions as $permission) {
      $providers[$permission['provider']] = $permission['provider'];
    }

    $form['label'] = [
      '#type' => 'select',
      '#title' => $this->t('Role'),
      '#options' => $defined_roles,
      '#default_value' => [$permission_filter_by_role->label()],
      '#multiple' => FALSE
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $permission_filter_by_role->id(),
      '#machine_name' => [
        'exists' => '\Drupal\pfr\Entity\PermissionFilterByRole::load',
      ],
      '#disabled' => !$permission_filter_by_role->isNew(),
    ];

    $form['module_permissions'] = [
      '#type' => 'select',
      '#title' => $this->t('Modules Access'),
      '#options' => [self::ALL_OPTIONS => '--All Modules'] + [self::NONE_OPTIONS => '--None Options'] + $providers,
      '#default_value' => explode("," , $permission_filter_by_role->getModulePermissions()),
      '#multiple' => TRUE
    ];

    $form['roles_permissions'] = [
      '#type' => 'select',
      '#title' => $this->t('Role Access'),
      '#options' => [self::ALL_OPTIONS => '--All Roles'] + [self::NONE_OPTIONS => '--None Options'] + $defined_roles,
      '#default_value' => explode("," , $permission_filter_by_role->getRolesPermissions()),
      '#multiple' => TRUE
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $permission_filter_by_role = $this->entity;
    $selected_modules = implode("," , $permission_filter_by_role->getModulePermissions());
    $selected_roles = implode("," , $permission_filter_by_role->getRolesPermissions());
    $permission_filter_by_role->setModulePermissions($selected_modules);
    $permission_filter_by_role->setRolesPermissions($selected_roles);
    $status = $permission_filter_by_role->save();
    switch ($status) {
      case SAVED_NEW:
        $this->messenger()->addMessage($this->t('Created the %label Permission filter by role.', [
          '%label' => $permission_filter_by_role->label(),
        ]));
        break;

      default:
        $this->messenger()->addMessage($this->t('Saved the %label Permission filter by role.', [
          '%label' => $permission_filter_by_role->label(),
        ]));
    }
    $form_state->setRedirectUrl($permission_filter_by_role->toUrl('collection'));
  }

}
