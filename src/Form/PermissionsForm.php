<?php

namespace Drupal\pfr\Form;

use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\KeyValueStore\KeyValueStoreExpirableInterface;
use Drupal\user\Entity\User;
use Drupal\user\Form\UserPermissionsForm;
use Drupal\user\PermissionHandlerInterface;
use Drupal\user\RoleStorageInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides an enhanced user permissions administration form.
 */
class PermissionsForm extends UserPermissionsForm {

  /**
   * Indicates that all options should be user for filter.
   */
  const ALL_OPTIONS = '-1';
  const NONE_OPTIONS = '-2';

  /**
   * The expirable key value store.
   *
   * @var \Drupal\Core\KeyValueStore\KeyValueStoreExpirableInterface
   */
  protected $keyValueExpirable;

  /**
   * Constructs a new PermissionsForm.
   *
   * @param \Drupal\user\PermissionHandlerInterface $permission_handler
   *   The permission handler.
   * @param \Drupal\user\RoleStorageInterface $role_storage
   *   The role storage.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   * @param \Drupal\Core\KeyValueStore\KeyValueStoreExpirableInterface $key_value_expirable
   *   The key value expirable factory.
   */
  public function __construct(PermissionHandlerInterface $permission_handler, RoleStorageInterface $role_storage, ModuleHandlerInterface $module_handler, KeyValueStoreExpirableInterface $key_value_expirable) {
    parent::__construct($permission_handler, $role_storage, $module_handler);

    $this->keyValueExpirable = $key_value_expirable;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('user.permissions'),
      $container->get('entity_type.manager')->getStorage('user_role'),
      $container->get('module_handler'),
      $container->get('keyvalue.expirable')->get('filter_perms_list')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $this->submitFormFilter($form, $form_state);
    // Render role/permission overview:
    $hide_descriptions = system_admin_compact_mode();

    $form['system_compact_link'] = [
      '#id' => FALSE,
      '#type' => 'system_compact_link',
    ];

    $permissions = $this->permissionHandler->getPermissions();

    $providers = [];
    foreach ($permissions as $permission) {
      $providers[$permission['provider']] = $permission['provider'];
    }

    $roles = $this->getRoles();

    $defined_roles = [];
    foreach ($roles as $role_name => $role) {
      $defined_roles[$role_name] = $role->label();
    }

    $filter = $this->getFilterSettings();

    $role_names = $role_permissions = $admin_roles = [];

    foreach ($roles as $role_name => $role) {
      if (in_array(self::ALL_OPTIONS, $filter['roles']) || in_array($role_name, $filter['roles'])) {
        // Retrieve role names for columns.
        $role_names[$role_name] = $role->label();
        // Fetch permissions for the roles.
        $role_permissions[$role_name] = $role->getPermissions();
        $admin_roles[$role_name] = $role->isAdmin();
      }
    }

    // Store $role_names for use when saving the data.
    $form['role_names'] = [
      '#type' => 'value',
      '#value' => $role_names,
    ];

    $permissions_by_provider = [];
    foreach ($permissions as $permission_name => $permission) {
      if (in_array(self::ALL_OPTIONS, $filter['modules']) || in_array($permission['provider'], $filter['modules'])) {
        $permissions_by_provider[$permission['provider']][$permission_name] = $permission;
      }
    }

    $form['permissions'] = [
      '#type' => 'table',
      '#header' => [$this->t('Permission')],
      '#id' => 'permissions',
      '#attributes' => ['class' => ['permissions', 'js-permissions']],
      '#sticky' => TRUE,
      '#empty' => $this->t("You are not allowed to see permissions yet, please contact to project's Admin"),
    ];

    // Only build the rest of the form if there are any filter settings.
    if (empty($role_names) || empty($permissions_by_provider)) {
      return $form;
    }

    foreach ($role_names as $name) {
      $form['permissions']['#header'][] = [
        'data' => $name,
        'class' => ['checkbox'],
      ];
    }

    foreach ($permissions_by_provider as $provider => $permissions) {
      // Module name.
      $form['permissions'][$provider] = [
        [
          '#wrapper_attributes' => [
            'colspan' => count($role_names) + 1,
            'class' => ['module'],
            'id' => 'module-' . $provider,
          ],
          '#markup' => $this->moduleHandler->getName($provider),
        ],
      ];
      foreach ($permissions as $perm => $perm_item) {
        // Fill in default values for the permission.
        $perm_item += [
          'description' => '',
          'restrict access' => FALSE,
          'warning' => !empty($perm_item['restrict access']) ? $this->t('Warning: Give to trusted roles only; this permission has security implications.') : '',
        ];
        $form['permissions'][$perm]['description'] = [
          '#type' => 'inline_template',
          '#template' => '<div class="permission"><span class="title">{{ title }}</span>{% if description or warning %}<div class="description">{% if warning %}<em class="permission-warning">{{ warning }}</em> {% endif %}{{ description }}</div>{% endif %}</div>',
          '#context' => [
            'title' => $perm_item['title'],
          ],
        ];
        // Show the permission description.
        if (!$hide_descriptions) {
          $form['permissions'][$perm]['description']['#context']['description'] = $perm_item['description'];
          $form['permissions'][$perm]['description']['#context']['warning'] = $perm_item['warning'];
        }
        foreach ($role_names as $rid => $name) {
          $form['permissions'][$perm][$rid] = [
            '#title' => $name . ': ' . $perm_item['title'],
            '#title_display' => 'invisible',
            '#wrapper_attributes' => [
              'class' => ['checkbox'],
            ],
            '#type' => 'checkbox',
            '#default_value' => in_array($perm, $role_permissions[$rid]) ? 1 : 0,
            '#attributes' => ['class' => ['rid-' . $rid, 'js-rid-' . $rid]],
            '#parents' => [$rid, $perm],
          ];
          // Show a column of disabled but checked checkboxes.
          if ($admin_roles[$rid]) {
            $form['permissions'][$perm][$rid]['#disabled'] = TRUE;
            $form['permissions'][$perm][$rid]['#default_value'] = TRUE;
          }
        }
      }
    }

    $form['actions'] = ['#type' => 'actions'];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save permissions'),
      '#button_type' => 'primary',
    ];

    $form['#attached']['library'][] = 'user/drupal.user.permissions';

    return $form;
  }

  /**
   * Saves the roles and modules selection.
   */
  public function submitFormFilter(array &$form, FormStateInterface $form_state) {
    $current_user = \Drupal::currentUser();
    $roles = $current_user->getRoles();
    $permission = \Drupal::entityTypeManager()->getStorage('permission_filter_by_role')->load($roles[1]);
    if ($roles[1] == 'administrator') {
      $roles = [-1];
      $permissions = [-1];
      $this->saveFilterSettings($roles, $permissions);
    } else {
      if($permission){
        $roles = explode("," ,  $permission->getRolesPermissions());
        $permissions = explode("," , $permission->getModulePermissions());
        $this->saveFilterSettings($roles, $permissions);
      }
    }
  }

  /**
   * Saves the filter settings for the current user.
   *
   * @param array $roles
   *   The roles to filter by.
   * @param array $modules
   *   The modules to filter by.
   */
  protected function saveFilterSettings(array $roles, array $modules) {
    $values = ['roles' => $roles, 'modules' => $modules];
    $this->keyValueExpirable->setWithExpire($this->currentUser()->id(), $values, 60);
  }

  /**
   * Retrieve the filter settings for the current user.
   *
   * @return array
   *   The filter setting for the current user.
   */
  protected function getFilterSettings() {
    $default = ['roles' => [], 'modules' => []];
    return $this->keyValueExpirable->get($this->currentUser()->id(), $default);
  }

}
