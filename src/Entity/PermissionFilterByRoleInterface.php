<?php

namespace Drupal\pfr\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Permission filter by role entities.
 */
interface PermissionFilterByRoleInterface extends ConfigEntityInterface {

  /**
   * Returns The Module Access Permissions
   *
   * @return string
   *
  The Module access permissions
   */
  public function getModulePermissions();

  /**
   * Returns The Roles Access Permissions
   *
   * @return string
   *
  The Roles access permissions
   */
  public function getRolesPermissions();

  /**
   * Set the Permissions
   *
   * @return string
   *
  Set the permissions
   */
  public function setModulePermissions($module_permissions);

  /**
   * Set the Role Access Permissions
   *
   * @return string
   *
  Set Role Access Permissions the permissions
   */
  public function setRolesPermissions($roles_permissions);
}
