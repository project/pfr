<?php

namespace Drupal\pfr\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;

/**
 * Defines the Permission filter by role entity.
 *
 * @ConfigEntityType(
 *   id = "permission_filter_by_role",
 *   label = @Translation("Permission filter by role"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\pfr\PermissionFilterByRoleListBuilder",
 *     "form" = {
 *       "add" = "Drupal\pfr\Form\PermissionFilterByRoleForm",
 *       "edit" = "Drupal\pfr\Form\PermissionFilterByRoleForm",
 *       "delete" = "Drupal\pfr\Form\PermissionFilterByRoleDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\pfr\PermissionFilterByRoleHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "permission_filter_by_role",
 *   admin_permission = "administer site configuration",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   config_export = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid",
 *     "module_permissions" = "module_permissions",
 *     "roles_permissions" = "roles_permissions"
 *   },
 *   links = {
 *     "canonical" = "/admin/config/people/permission_filter_by_role/{permission_filter_by_role}",
 *     "add-form" = "/admin/config/people/permission_filter_by_role/add",
 *     "edit-form" = "/admin/config/people/permission_filter_by_role/{permission_filter_by_role}/edit",
 *     "delete-form" = "/admin/config/people/permission_filter_by_role/{permission_filter_by_role}/delete",
 *     "collection" = "/admin/config/people/permission_filter_by_role"
 *   }
 * )
 */
class PermissionFilterByRole extends ConfigEntityBase implements PermissionFilterByRoleInterface {

  /**
   * The Permission filter by role ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Permission filter by role label.
   *
   * @var string
   */
  protected $label;

  /**
   * The Module access permissions
   *
   * @var string
   */
  protected $module_permissions;

  /**
   * Returns The Roles Access Permissions
   *
   * @var string
   */
  protected $roles_permissions;

  public function getModulePermissions() {
    return $this->module_permissions;
  }

  public function getRolesPermissions() {
    return $this->roles_permissions;
  }

  public function setModulePermissions($module_permissions) {
    $this->module_permissions = $module_permissions;
    return $this;
  }

  public function setRolesPermissions($roles_permissions){
    $this->roles_permissions = $roles_permissions;
    return $this;
  }

}
